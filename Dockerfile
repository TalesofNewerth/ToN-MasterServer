FROM trafex/php-nginx

# copying the source directory and install the dependencies with composer

COPY --chown=nginx . /var/www/html

EXPOSE 8080

ENTRYPOINT ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]